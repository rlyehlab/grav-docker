# Grav docker

This is a very simple docker for [Grav](https://getgrav.org). However, due to
the way grav works, putting the files inside the container is not possible. For
that, I added the Grav repo as a submodule and as a volume to the container.

## Deploy

Clone this repo, run `git submodule init && git submodule update` to initialize
the Grav submodule, then go into the `grav` directory and get the latest changes:
`git pull`, then check out the desired tag, i.e.: `git checkout 1.5.7`.

Once Grav is set up, you need to chown all of its files as the user/group
number 82 (which the one that runs in the container): `chown -R 82:82 grav`.

Edit `nginx.conf` file to set the `server_name` properly to the site URL.

Finally, launch docker: `sudo docker-compose up -d`. Follow then the
instructions to configure Grav.

By default, it will launch at the port 9007.

## License

[![Creative Commons
License](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/) *Grav Docker* by [HacKan](https://hackan.net) is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International
License](https://creativecommons.org/licenses/by-sa/4.0/).

