FROM php:7-fpm-alpine
LABEL maintainer="HacKan CuBa <hackan@rlab.be> (@hackancuba)"

# Inspired and adapted from https://github.com/unifyprojects/docker-grav

# Install dependencies
RUN apk update \
    && apk add --no-cache \
        libzip-dev \
        freetype-dev \
        libjpeg-turbo-dev \
        libpng-dev \
        yaml-dev \
        autoconf \
        build-base \
        gcc \
    && docker-php-ext-install opcache \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install zip \
    && rm -rf /var/cache/apk/*

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN printf '%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n' \
    'opcache.memory_consumption=128' \
    'opcache.interned_strings_buffer=8' \
    'opcache.max_accelerated_files=4000' \
	'opcache.revalidate_freq=2' \
	'opcache.fast_shutdown=1' \
	'opcache.enable_cli=1' \
	'upload_max_filesize=128M' \
	'post_max_size=128M' \
	> /usr/local/etc/php/conf.d/php-recommended.ini

RUN pecl install apcu \
    && pecl install yaml \
    && docker-php-ext-enable apcu yaml

RUN mkdir -p /var/www/html

# Drop privs
USER www-data

WORKDIR /var/www/html
